//
//  HomeViewModelTests.swift
//  EasyTest
//
//  Created by Lucas Marçal on 30/08/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

import XCTest
import RxSwift
@testable import EasyTest

class HomeViewModelTests: XCTestCase {
    var viewModel = HomeViewModel()
    var disposeBag:DisposeBag!
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        disposeBag = nil
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testGetCars(){
        viewModel.getCabs().subscribe(onNext:{ value in
            print(value.count)
            XCTAssertTrue(value.count != 0, "Para retornar no onNext, o array precisa ter pelo menos um item")
        }).addDisposableTo(disposeBag)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
