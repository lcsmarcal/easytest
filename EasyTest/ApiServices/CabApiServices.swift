//
//  CabApiServices.swift
//  EasyTest
//
//  Created by Lucas Marçal on 28/08/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

import RxAlamofire
import RxSwift
import SwiftyJSON
import Alamofire
import CoreLocation

class CabApiServices{
    static let sharedInstance = CabApiServices()
    let disposeBag = DisposeBag()
    var cabs:[CarModel] = []
    
    func fetchCabsAround(latitude: Double, longitude: Double) -> Observable<[CarModel]>{
        
        guard NetworkReachabilityManager()!.isReachable != false else {
            return Observable.error(EasyError.noInternetConnection)
        }
        
        let parameters = ["lat":"\(latitude)", "lng":"\(longitude)"]
        let url = URL(string:"http://quasinada-ryu.easytaxi.net.br/api/gettaxis")!
        
        return RxAlamofire.requestJSON(.get, url, parameters: parameters, headers: [:])
        .catchError({ error -> Observable<(HTTPURLResponse, Any)> in
            return Observable.error(error)
        })
        .map{(response, json) -> [CarModel] in
            var cars:[CarModel] = []
            
            let js = JSON(json)
            if let taxis = js["taxis"].array{
                for taxi in taxis{
                    cars.append(CarModel(json: taxi))
                }
            }
            
            guard cars.count > 0 else{
                throw EasyError.noCarsAround
            }
            
            self.cabs = cars
            return cars
        }
        
    }
    
    func requestCab(latitude: Double, longitude: Double) -> Observable<CarModel>{
        let userLocation = CLLocation(latitude: latitude, longitude: longitude)
        let nearestCar = cabs.reduce(cabs.first, {($0!.location!.distance(from: userLocation) < $1.location!.distance(from: userLocation)) ? $0 : $1})
        
        if let car = nearestCar{
            return Observable.just(car).delay(1.5, scheduler: MainScheduler.instance)
        }else{
            return Observable.error(EasyError.noCarsAround)
        }
        
    }
    
    
}
