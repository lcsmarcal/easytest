//
//  HomeViewController.swift
//  EasyTest
//
//  Created by Lucas Marcal on 24/08/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import GoogleMaps
import GooglePlaces
import Permission
import RxPermission

class HomeViewController: UIViewController{

    @IBOutlet weak var mapView: GMSMapView!{
        didSet{
            mapView.delegate = self
        }
    }
    @IBOutlet weak var locationButton: UIButton!{
        didSet{
            locationButton.layer.cornerRadius = 8.0
            locationButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var centerPoint: UIImageView!
    @IBOutlet weak var requestButton: UIButton!{
        didSet{
            requestButton.backgroundColor = .white
            requestButton.layer.cornerRadius = 0.25
            requestButton.layer.masksToBounds = true
            requestButton.titleLabel?.textColor = .black
        }
    }

    @IBOutlet weak var addressLabel: UILabel!{
        didSet{
            addressLabel.backgroundColor = .white
        }
    }
    
    let disposeBag = DisposeBag()
    let locationManager = CLLocationManager()
    let viewModel = HomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.bringSubview(toFront: requestButton)
        mapView.bringSubview(toFront: addressLabel)
        mapView.bringSubview(toFront: centerPoint)
        mapView.bringSubview(toFront: locationButton)
        
        bindPermissionCheck()
        bindVariables()
        bindUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bindPermissionCheck(){
        Permission.locationWhenInUse
            .rx.permission
            .subscribe(onNext:{ [unowned self] status in
                switch(status){
                case .authorized:
                    self.startUpdateLocation()
                    break
                    
                case .notDetermined:
                    break
                    
                case .denied:
                    break
                    
                case .disabled:
                    break
                }
            }).addDisposableTo(disposeBag)
    }
    
    func bindVariables(){
        viewModel.address.asObservable()
            .skip(1)
            .debounce(0.7, scheduler: MainScheduler.instance)
            .subscribe(onNext:{[unowned self] address in
                self.addressLabel.text = address?.thoroughfare
                self.getCabs()
            }).addDisposableTo(disposeBag)
    }
    
    func bindUI(){
        requestButton.rx.tap
            .throttle(4, scheduler: MainScheduler.instance)
            .subscribe(onNext:{ [unowned self] _ in
                self.requestCab()
            }).addDisposableTo(disposeBag)
        
        locationButton.rx.tap
            .subscribe(onNext:{ [unowned self] _ in
                self.startUpdateLocation()
            }).addDisposableTo(disposeBag)
    }
    
    func startUpdateLocation(){
        if CLLocationManager.locationServicesEnabled(){
            mapView.isMyLocationEnabled = true
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }else{
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func updateMapCamera(coordinates: CLLocationCoordinate2D){
        let camera = GMSCameraPosition.camera(withLatitude:coordinates.latitude, longitude:coordinates.longitude, zoom: 17.0)
        mapView?.animate(to: camera)
    }
    
    func reverseGeocoding(coordinates: CLLocationCoordinate2D){
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinates) { [unowned self] (response, error) in
            guard response != nil else{
                if let err = error{
                    self.handleError(erro: err)
                }
                return
            }
           self.viewModel.address.value = response?.firstResult()
        }
    }
    
    func getCabs(){
        viewModel.getCabs().subscribe(
            onNext:{[unowned self] value in
                self.updateMap()
            },
            onError:{[unowned self] error in
                self.handleError(erro: error)
            })
        .addDisposableTo(disposeBag)
    }
    
    func requestCab(){
        let loadView = loadingView()
        mapView.addSubview(loadView)
        viewModel.requestCab().subscribe(onNext:{[unowned self] _ in
            loadView.removeFromSuperview()
            self.updateMap()
        }).addDisposableTo(disposeBag)
    }
    
    func updateMap(){
        self.mapView.clear()
        for car in viewModel.cabs.value{
            let position = CLLocationCoordinate2D(latitude: car.lat, longitude: car.lng)
            let marker = GMSMarker(position: position)
            marker.icon = #imageLiteral(resourceName: "car")
            marker.map = mapView
        }
    }
    
    func handleError(erro: Error){
        var msg = ""
        switch erro {
        case EasyError.noInternetConnection:
            msg = "Connection Problem"
        case EasyError.noCarsAround:
            msg = "Não temos nenhum carro disponivel"
        case EasyError.noAddressSelected:
            msg = "Nenhum endereço selecionado"
        default:
            msg = erro.localizedDescription
        }
        let alert = UIAlertController(title: "Erro", message: msg , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    
    func loadingView() -> UIView{
        let overlayView = UIView(frame: view.bounds)
        overlayView.backgroundColor = .black
        overlayView.alpha = 0.7
        
        
        let label = UILabel(frame: CGRect(x: 0, y: 100, width: overlayView.frame.width, height: 50))
        label.textColor = .white
        label.text = "Carregando"
        label.textAlignment = .center
        
        overlayView.addSubview(label)
        
        return overlayView
    }
    
}

extension HomeViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{ 
            updateMapCamera(coordinates: location.coordinate)
            manager.stopUpdatingLocation()
        }
    }
}

extension HomeViewController: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        reverseGeocoding(coordinates: mapView.camera.target)
    }
}
