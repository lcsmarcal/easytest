//
//  NetworkError.swift
//  EasyTest
//
//  Created by Lucas Marçal on 28/08/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//



enum EasyError: Error {
    case noInternetConnection
    case noAddressSelected
    case apiError
    case noCarsAround
}
