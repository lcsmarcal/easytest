//
//  HomeViewModel.swift
//  EasyTest
//
//  Created by Lucas Marcal on 24/08/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//

import GoogleMaps
import RxSwift

class HomeViewModel{
    var address = Variable<GMSAddress?>(nil)
    var cabs:Variable<[CarModel]> = Variable([])
    
    func getCabs() -> Observable<[CarModel]>{
        guard address.value != nil else{
            return Observable.error(EasyError.noAddressSelected)
        }
        return CabApiServices.sharedInstance.fetchCabsAround(latitude: address.value!.coordinate.latitude, longitude: address.value!.coordinate.longitude).map({[unowned self] cars in
            self.cabs.value = cars
            return cars
        })
    }
    
    func requestCab() -> Observable<CarModel>{
        guard address.value != nil else{
            return Observable.error(EasyError.noAddressSelected)
        }
        return CabApiServices.sharedInstance.requestCab(latitude: address.value!.coordinate.latitude, longitude: address.value!.coordinate.longitude).map({[unowned self] car in
            self.cabs.value = [car]
            return car
        })
    }
    
}
