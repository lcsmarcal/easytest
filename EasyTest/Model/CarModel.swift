//
//  CarModel.swift
//  EasyTest
//
//  Created by Lucas Marçal on 28/08/17.
//  Copyright © 2017 Lucas Marcal. All rights reserved.
//
import SwiftyJSON
import CoreLocation

class CarModel{
    var driverCar = ""
    var driverName = ""
    var lat:Double = 0
    var lng:Double = 0
    var location: CLLocation?
    init(json: JSON){
        if let driverCar = json["driver-car"].string{
            self.driverCar = driverCar
        }
        
        if let driverName = json["driver-name"].string{
            self.driverName = driverName
        }
        
        if let lat = json["lat"].double{
            self.lat = lat
        }
        
        if let lng = json["lng"].double{
            self.lng = lng
        }
        
        self.location = CLLocation(latitude: lat, longitude: lng)
    }
}
